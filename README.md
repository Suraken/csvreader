Create a class or group of classes that takes the “data.csv” data file (a part of the test) as
an input and load the data to the memory, ensure that it is easy to read and sort, and
enable it to be exported to HTML. This class or group must enable the data to be exported
easily in other formats (e.g., CSV and Excel).
